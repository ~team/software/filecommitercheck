#!/usr/bin/python3
"""docstring todo"""
import argparse
import os
import subprocess
from pathlib import Path
from dataclasses import dataclass
from collections import defaultdict, OrderedDict
import re


@dataclass
class Commit:
    """docstring todo"""
    _commits = dict()
    _id: str
    _author: str
    _date: str
    _message: str
    _nb_lines_added: int

    def __post_init__(self):
        Commit._commits[self._id] = self
        # if not _author:
            # print(f'commit {_id} not Authored')

    def __str__(self) -> str:
        return f'Commit {self._id}\n' +\
               f'Message: "{self._message}"\n' +\
               f'Nb lines added: {self._nb_lines_added}\n' +\
               f'Date: {self._date}'


def get_args() -> object:
    """use argparse to obtain the app cmd line args"""
    _description = 'Check contributors who contributed to more '\
                   'than 10 lines to a git file.'
    _example = 'example: python fileCommiterCheck.py fullFilePath'
    _parser = argparse.ArgumentParser(description=_description,
                                      epilog=_example)
    _help = 'Full path of the file to check'
    _parser.add_argument('file_full_path', type=str,
                         nargs=1, help=_help)
    _help = 'Author name to get commits from for this file'
    _parser.add_argument('-A', '--author', help=_help)
    _args = _parser.parse_args()
    return _args


def get_commits() -> None:
    """docstring todo"""
    _subprocess = subprocess.run(["git", "log", "-p",
                                 args.file_full_path[0]],
                                 capture_output=True,
                                 check=True)
    _cmd_result = _subprocess.stdout.decode()
    _commits = _cmd_result.split('commit ')[1:]
    for _commit in _commits:
        _commit_lines = _commit.splitlines()
        _id = str(_commit_lines[0].strip())
        _author = _commit_lines[1].split('Author: ')[1].strip()
        _date = _commit_lines[2].split('Date: ')[1].strip()
        _message = re.sub(' +', ' ', _commit.split('\n\n')[1]
                          .split('diff --git')[0]
                          .replace('\n', '').strip())
        _subprocess = subprocess.run(["git", "show", "--stat", str(_id)],
                                     capture_output=True,
                                     check=True)
        _cmd_result = _subprocess.stdout.decode()
        try:
            _modifications =\
                _cmd_result.split('file changed, ')[1].split('\n')[0].strip()
        except IndexError:
            _modifications =\
                _cmd_result.split('files changed, ')[1].split('\n')[0].strip()
        _nb_lines_added = int(_modifications.split('insertion')[0])
        Commit(_id, _author, _date, _message, _nb_lines_added)


args = get_args()
FTC = Path(args.file_full_path[0])
if FTC.is_file():
    os.chdir(args.file_full_path[0].rsplit('/', 1)[0])
    get_commits()
else:
    raise FileNotFoundError('Check if the file exists and give the full path of the file to check for commits as arg')
nb_commits_for_this_file = len(Commit._commits.items())
print(f'\nThere is {nb_commits_for_this_file} commit(s) for this file:\n')
commits_by_author = defaultdict(list)
for k in reversed(Commit._commits):
    if args.author:
        if args.author in Commit._commits[k]._author:
            commits_by_author[Commit._commits[k]._author].append(k)
    else:
        commits_by_author[Commit._commits[k]._author].append(k)
for k, v in commits_by_author.items():
    to_print = ''
    nb_commits_by_author = 0
    nb_lines_inserted_by_this_author = 0
    for commit_id in v:
        nb_commits_by_author += 1
        nb_lines_inserted_by_this_author += Commit._commits[commit_id]._nb_lines_added
        if not args.author:
            to_print += f'{commit_id} -> {Commit._commits[commit_id]._message}\n'
            to_print += Commit._commits[commit_id]._date
        else:
           to_print +=  Commit._commits[commit_id].__str__()
           to_print += '\n'
    if nb_lines_inserted_by_this_author >= 10:
        print(f'\n{k} inserted {nb_lines_inserted_by_this_author} lines '
              f'in {nb_commits_by_author} commit(s):')
        if not args.author:
            print('commit(s) id(s)/message(s) are:')
        print(to_print)
